# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'interface.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(811, 371)
        MainWindow.setStyleSheet("QMainWindow{\n"
                                 "    background-color: #E1E6C6;\n"
                                 "}")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_10 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_10.setSpacing(6)
        self.horizontalLayout_10.setObjectName("horizontalLayout_10")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy)
        self.label_3.setMinimumSize(QtCore.QSize(360, 25))
        self.label_3.setMaximumSize(QtCore.QSize(360, 25))
        self.label_3.setStyleSheet("QLabel{\n"
                                   "    color: #1D2017;\n"
                                   "    font: 18pt \"Candara\";\n"
                                   "}\n"
                                   "")
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_10.addWidget(self.label_3)
        self.verticalLayout.addLayout(self.horizontalLayout_10)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setMinimumSize(QtCore.QSize(35, 25))
        self.label.setMaximumSize(QtCore.QSize(35, 25))
        self.label.setStyleSheet("QLabel{\n"
                                 "    color: #1D2017;\n"
                                 "    font: 12pt \"Candara\";\n"
                                 "}\n"
                                 "")
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEdit.sizePolicy().hasHeightForWidth())
        self.lineEdit.setSizePolicy(sizePolicy)
        self.lineEdit.setMinimumSize(QtCore.QSize(290, 30))
        self.lineEdit.setMaximumSize(QtCore.QSize(290, 30))
        self.lineEdit.setStyleSheet("QLineEdit{\n"
                                    "    padding: 5px;\n"
                                    "    color: #1D2017;\n"
                                    "    font: 12pt \"Candara\";\n"
                                    "    border-radius: 5px;\n"
                                    "}")
        self.lineEdit.setClearButtonEnabled(False)
        self.lineEdit.setObjectName("lineEdit")
        self.horizontalLayout.addWidget(self.lineEdit)
        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_4.sizePolicy().hasHeightForWidth())
        self.pushButton_4.setSizePolicy(sizePolicy)
        self.pushButton_4.setMinimumSize(QtCore.QSize(30, 30))
        self.pushButton_4.setMaximumSize(QtCore.QSize(30, 30))
        self.pushButton_4.setStyleSheet("QPushButton{\n"
                                        "    background-color: #AFC073;\n"
                                        "    border: none;\n"
                                        "    padding: 5px;\n"
                                        "    color: #1D2017;\n"
                                        "    font: 12pt \"Candara\";\n"
                                        "    border-radius: 15px;\n"
                                        "}\n"
                                        "\n"
                                        "QPushButton:hover{\n"
                                        "    background-color: #b0cc4e;\n"
                                        "}\n"
                                        "")
        self.pushButton_4.setObjectName("pushButton_4")
        self.horizontalLayout.addWidget(self.pushButton_4)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setSpacing(6)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.checkBox = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBox.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.checkBox.sizePolicy().hasHeightForWidth())
        self.checkBox.setSizePolicy(sizePolicy)
        self.checkBox.setMinimumSize(QtCore.QSize(60, 20))
        self.checkBox.setMaximumSize(QtCore.QSize(60, 20))
        self.checkBox.setStyleSheet("QCheckBox{\n"
                                    "    color: #1D2017;\n"
                                    "    font: 12pt \"Candara\";\n"
                                    "}\n"
                                    "QCheckBox::indicator {\n"
                                    "    width: 14px;\n"
                                    "    height: 14px;\n"
                                    "}")
        self.checkBox.setChecked(True)
        self.checkBox.setObjectName("checkBox")
        self.horizontalLayout_2.addWidget(self.checkBox)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_4.sizePolicy().hasHeightForWidth())
        self.label_4.setSizePolicy(sizePolicy)
        self.label_4.setMinimumSize(QtCore.QSize(55, 20))
        self.label_4.setMaximumSize(QtCore.QSize(55, 20))
        self.label_4.setStyleSheet("QLabel{\n"
                                   "    color: #1D2017;\n"
                                   "    font: 12pt \"Candara\";\n"
                                   "}\n"
                                   "")
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_2.addWidget(self.label_4)
        self.lineEdit_3 = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEdit_3.sizePolicy().hasHeightForWidth())
        self.lineEdit_3.setSizePolicy(sizePolicy)
        self.lineEdit_3.setMinimumSize(QtCore.QSize(75, 30))
        self.lineEdit_3.setMaximumSize(QtCore.QSize(75, 30))
        self.lineEdit_3.setStyleSheet("QLineEdit{\n"
                                      "    padding: 5px;\n"
                                      "    color: #1D2017;\n"
                                      "    font: 12pt \"Candara\";\n"
                                      "    border-radius: 5px;\n"
                                      "}")
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.horizontalLayout_2.addWidget(self.lineEdit_3)
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy)
        self.label_5.setMinimumSize(QtCore.QSize(55, 20))
        self.label_5.setMaximumSize(QtCore.QSize(55, 20))
        self.label_5.setStyleSheet("QLabel{\n"
                                   "    color: #1D2017;\n"
                                   "    font: 12pt \"Candara\";\n"
                                   "}\n"
                                   "")
        self.label_5.setObjectName("label_5")
        self.horizontalLayout_2.addWidget(self.label_5)
        self.lineEdit_4 = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEdit_4.sizePolicy().hasHeightForWidth())
        self.lineEdit_4.setSizePolicy(sizePolicy)
        self.lineEdit_4.setMinimumSize(QtCore.QSize(75, 30))
        self.lineEdit_4.setMaximumSize(QtCore.QSize(75, 30))
        self.lineEdit_4.setStyleSheet("QLineEdit{\n"
                                      "    padding: 5px;\n"
                                      "    color: #1D2017;\n"
                                      "    font: 12pt \"Candara\";\n"
                                      "    border-radius: 5px;\n"
                                      "}")
        self.lineEdit_4.setObjectName("lineEdit_4")
        self.horizontalLayout_2.addWidget(self.lineEdit_4)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton.sizePolicy().hasHeightForWidth())
        self.pushButton.setSizePolicy(sizePolicy)
        self.pushButton.setMinimumSize(QtCore.QSize(180, 40))
        self.pushButton.setMaximumSize(QtCore.QSize(180, 40))
        self.pushButton.setStyleSheet("QPushButton{\n"
                                      "    background-color: #AFC073;\n"
                                      "    border: none;\n"
                                      "    padding: 5px;\n"
                                      "    color: #1D2017;\n"
                                      "    font: 12pt \"Candara\";\n"
                                      "    border-radius: 5px;\n"
                                      "}\n"
                                      "\n"
                                      "QPushButton:hover{\n"
                                      "    background-color: #b0cc4e;\n"
                                      "}\n"
                                      "QPushButton:pressed{\n"
                                      "    background-color: #AFC073;\n"
                                      "}")
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout_5.addWidget(self.pushButton)
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_2.sizePolicy().hasHeightForWidth())
        self.pushButton_2.setSizePolicy(sizePolicy)
        self.pushButton_2.setMinimumSize(QtCore.QSize(180, 30))
        self.pushButton_2.setMaximumSize(QtCore.QSize(180, 40))
        self.pushButton_2.setStyleSheet("QPushButton{\n"
                                        "    background-color: #AFC073;\n"
                                        "    border: none;\n"
                                        "    padding: 5px;\n"
                                        "    color: #1D2017;\n"
                                        "    font: 12pt \"Candara\";\n"
                                        "    border-radius: 5px;\n"
                                        "}\n"
                                        "\n"
                                        "QPushButton:hover{\n"
                                        "    background-color: #b0cc4e;\n"
                                        "}\n"
                                        "QPushButton:pressed{\n"
                                        "    background-color: #AFC073;\n"
                                        "}")
        self.pushButton_2.setObjectName("pushButton_2")
        self.horizontalLayout_5.addWidget(self.pushButton_2)
        self.verticalLayout.addLayout(self.horizontalLayout_5)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        self.label_2.setMinimumSize(QtCore.QSize(35, 25))
        self.label_2.setMaximumSize(QtCore.QSize(35, 25))
        self.label_2.setStyleSheet("QLabel{\n"
                                   "    color: #1D2017;\n"
                                   "    font: 12pt \"Candara\";\n"
                                   "}\n"
                                   "")
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_6.addWidget(self.label_2)
        self.lineEdit_2 = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEdit_2.sizePolicy().hasHeightForWidth())
        self.lineEdit_2.setSizePolicy(sizePolicy)
        self.lineEdit_2.setMinimumSize(QtCore.QSize(330, 30))
        self.lineEdit_2.setMaximumSize(QtCore.QSize(330, 30))
        self.lineEdit_2.setStyleSheet("QLineEdit{\n"
                                      "    padding: 5px;\n"
                                      "    color: #1D2017;\n"
                                      "    font: 12pt \"Candara\";\n"
                                      "    border-radius: 5px;\n"
                                      "}")
        self.lineEdit_2.setReadOnly(True)
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.horizontalLayout_6.addWidget(self.lineEdit_2)
        self.verticalLayout.addLayout(self.horizontalLayout_6)
        self.horizontalLayout_9 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_9.setObjectName("horizontalLayout_9")
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_3.sizePolicy().hasHeightForWidth())
        self.pushButton_3.setSizePolicy(sizePolicy)
        self.pushButton_3.setMinimumSize(QtCore.QSize(190, 40))
        self.pushButton_3.setMaximumSize(QtCore.QSize(190, 40))
        self.pushButton_3.setStyleSheet("QPushButton{\n"
                                        "    background-color: #AFC073;\n"
                                        "    border: none;\n"
                                        "    padding: 5px;\n"
                                        "    color: #1D2017;\n"
                                        "    font: 12pt \"Candara\";\n"
                                        "    border-radius: 5px;\n"
                                        "}\n"
                                        "\n"
                                        "QPushButton:hover{\n"
                                        "    background-color: #b0cc4e;\n"
                                        "}\n"
                                        "QPushButton:pressed{\n"
                                        "    background-color: #AFC073;\n"
                                        "}")
        self.pushButton_3.setObjectName("pushButton_3")
        self.horizontalLayout_9.addWidget(self.pushButton_3)
        self.verticalLayout.addLayout(self.horizontalLayout_9)
        self.horizontalLayout_8.addLayout(self.verticalLayout)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy)
        self.widget.setMinimumSize(QtCore.QSize(410, 300))
        self.widget.setMaximumSize(QtCore.QSize(410, 300))
        self.widget.setObjectName("widget")
        self.verticalLayout_2.addWidget(self.widget)
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.pushButton_6 = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_6.sizePolicy().hasHeightForWidth())
        self.pushButton_6.setSizePolicy(sizePolicy)
        self.pushButton_6.setMinimumSize(QtCore.QSize(180, 40))
        self.pushButton_6.setMaximumSize(QtCore.QSize(180, 40))
        self.pushButton_6.setStyleSheet("QPushButton{\n"
                                        "    background-color: #AFC073;\n"
                                        "    border: none;\n"
                                        "    padding: 5px;\n"
                                        "    color: #1D2017;\n"
                                        "    font: 12pt \"Candara\";\n"
                                        "    border-radius: 5px;\n"
                                        "}\n"
                                        "\n"
                                        "QPushButton:hover{\n"
                                        "    background-color: #b0cc4e;\n"
                                        "}\n"
                                        "QPushButton:pressed{\n"
                                        "    background-color: #AFC073;\n"
                                        "}")
        self.pushButton_6.setObjectName("pushButton_6")
        self.horizontalLayout_7.addWidget(self.pushButton_6)
        self.pushButton_5 = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_5.sizePolicy().hasHeightForWidth())
        self.pushButton_5.setSizePolicy(sizePolicy)
        self.pushButton_5.setMinimumSize(QtCore.QSize(180, 40))
        self.pushButton_5.setMaximumSize(QtCore.QSize(180, 40))
        self.pushButton_5.setStyleSheet("QPushButton{\n"
                                        "    background-color: #AFC073;\n"
                                        "    border: none;\n"
                                        "    padding: 5px;\n"
                                        "    color: #1D2017;\n"
                                        "    font: 12pt \"Candara\";\n"
                                        "    border-radius: 5px;\n"
                                        "}\n"
                                        "\n"
                                        "QPushButton:hover{\n"
                                        "    background-color: #b0cc4e;\n"
                                        "}\n"
                                        "QPushButton:pressed{\n"
                                        "    background-color: #AFC073;\n"
                                        "}")
        self.pushButton_5.setObjectName("pushButton_5")
        self.horizontalLayout_7.addWidget(self.pushButton_5)
        self.verticalLayout_2.addLayout(self.horizontalLayout_7)
        self.horizontalLayout_8.addLayout(self.verticalLayout_2)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label_3.setText(_translate("MainWindow", "GraphAssistant"))
        self.label.setText(_translate("MainWindow", "f(x):"))
        self.pushButton_4.setText(_translate("MainWindow", "?"))
        self.checkBox.setText(_translate("MainWindow", "Сетка"))
        self.label_4.setText(_translate("MainWindow", "Обл. X:"))
        self.label_5.setText(_translate("MainWindow", "Обл. Y:"))
        self.pushButton.setText(_translate("MainWindow", "Отобразить функцию"))
        self.pushButton_2.setText(_translate("MainWindow", "Производная"))
        self.label_2.setText(_translate("MainWindow", "f\'(x):"))
        self.pushButton_3.setText(_translate("MainWindow", "Отобразить производную"))
        self.pushButton_6.setText(_translate("MainWindow", "Удалить последний"))
        self.pushButton_5.setText(_translate("MainWindow", "Очистить холст"))
