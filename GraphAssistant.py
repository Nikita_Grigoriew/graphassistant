import sys
import numpy as np
from matplotlib import pyplot as plt
from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox
from MyInterface import Ui_MainWindow
from MyMath import MathForPlot
from MplForWidget import MyMplCanvas
from matplotlib.backends.backend_qt5 import NavigationToolbar2QT as NavigationToolbar


# Наследуемся от виджета из PyQt5.QtWidgets и от класса с интерфейсом
class MyWidget(QMainWindow, Ui_MainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        # Метод для загрузки интерфейса из класса Ui_MainWindow,
        self.setupUi(self)

        self.setWindowTitle('GraphAssistant')
        self.lineEdit.setPlaceholderText('x**2 + 3*x - 4')
        self.lineEdit_3.setPlaceholderText('-10; 10')
        self.lineEdit_4.setPlaceholderText('-10; 10')

        # Создание холста и панели навигации
        self.fig = plt.figure()
        self.layout_for_canvas = QtWidgets.QVBoxLayout(self.widget)
        self.canvas = MyMplCanvas(self.fig)
        self.canvas.axes = self.canvas.figure.add_subplot(111)
        self.layout_for_canvas.addWidget(self.canvas)
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.layout_for_canvas.addWidget(self.toolbar)

        self.pushButton.clicked.connect(self.show_plot)
        self.pushButton_2.clicked.connect(self.calc_deriv)
        self.pushButton_3.clicked.connect(self.show_deriv)
        self.pushButton_4.setToolTip('Правила ввода функций:\n1. Все переменные выражаются через x\n2. Все '
                                     'математические операции выражаются через +, -, *, /, **\n3. Коррень квадратный '
                                     '= sqrt, число пи = pi, экспанента = exp(x)\n4. Тригонометрические функции: sin('
                                     'x), cos(x), tan(x), ctg(x)')
        self.pushButton_5.clicked.connect(self.clear_canvas)
        self.pushButton_6.clicked.connect(self.delete_last)
        QtWidgets.QToolTip.setFont(QtGui.QFont('Candara', 12))
        self.data_func = []

    # Функция для отображения графика, заданного формулой
    def show_plot(self):
        # Очистка холста
        self.canvas.axes.clear()
        # Проверка флага на добавление сетки
        if self.checkBox.isChecked():
            self.canvas.axes.grid()
        # Проверка на наличие ввода граничных значений оси X и Y
        if self.lineEdit_3.text() != '':
            try:
                self.canvas.axes.set(
                    xlim=(float(self.lineEdit_3.text().split('; ')[0]), float(self.lineEdit_3.text().split('; ')[1])))
            except:
                self.error_message('Граничные значения Х некорректны.')
        if self.lineEdit_4.text() != '':
            try:
                self.canvas.axes.set(
                    ylim=(float(self.lineEdit_4.text().split('; ')[0]), float(self.lineEdit_4.text().split('; ')[1])))
            except:
                self.error_message('Граничные значения Y некорректны.')
        # Получение функции для отображения
        math_obj = MathForPlot(self.lineEdit.text())
        try:
            res_func = math_obj.plot_graph()
        except:
            self.error_message('Некорректные данные.')
        else:
            self.data_func.append(res_func)
            # Отображение функций, находящихся в массиве
            x = np.linspace(-10, 10, num=200)
            try:
                for i in self.data_func:
                    self.canvas.axes.plot(x, i, linewidth=2.0)
            except:
                self.data_func.pop()
                self.error_message('Невозможно отобразить функцию.')
            else:
                self.canvas.draw()

    # Функция для получения производной функции
    def calc_deriv(self):
        math_obj = MathForPlot(self.lineEdit.text())
        try:
            self.lineEdit_2.setText(math_obj.derivative())
        except:
            self.error_message('Невозможно рассчитать производную функции.')

    # Функция для отображения графика производной функции на холсте
    def show_deriv(self):
        self.canvas.axes.clear()
        if self.checkBox.isChecked():
            self.canvas.axes.grid()
        if self.lineEdit_3.text() != '':
            try:
                self.canvas.axes.set(
                    xlim=(float(self.lineEdit_3.text().split('; ')[0]), float(self.lineEdit_3.text().split('; ')[1])))
            except:
                self.error_message('Граничные значения X некорректны.')
        if self.lineEdit_4.text() != '':
            try:
                self.canvas.axes.set(
                    ylim=(float(self.lineEdit_4.text().split('; ')[0]), float(self.lineEdit_4.text().split('; ')[1])))
            except:
                self.error_message('Граничные значения Y некорректны.')
        # Получение функции для отображения
        math_obj = MathForPlot(self.lineEdit_2.text())
        try:
            res_func = math_obj.plot_graph()
        except:
            self.error_message('Некорректные данные.')
        else:
            self.data_func.append(res_func)
            # Отображение функций, находящихся в массиве
            x = np.linspace(-10, 10, num=200)
            try:
                for i in self.data_func:
                    self.canvas.axes.plot(x, i, linewidth=2.0)
            except:
                self.data_func.pop()
                self.lineEdit_2.setText('')
                self.error_message('Невозможно отобразить функцию.')
            else:
                self.canvas.draw()

    # Функция удаления графика последней введенной функции
    def delete_last(self):
        try:
            self.data_func.pop()
        except:
            self.error_message('Не удалось удалить график последней введенной функции.')
        else:
            self.canvas.axes.clear()
            if self.checkBox.isChecked():
                self.canvas.axes.grid()
            if self.lineEdit_3.text() != '':
                try:
                    self.canvas.axes.set(
                        xlim=(float(self.lineEdit_3.text().split('; ')[0]), float(self.lineEdit_3.text().split('; ')[1])))
                except:
                    self.error_message('Граничные значения X некорректны.')
            if self.lineEdit_4.text() != '':
                try:
                    self.canvas.axes.set(
                        ylim=(float(self.lineEdit_4.text().split('; ')[0]), float(self.lineEdit_4.text().split('; ')[1])))
                except:
                    self.error_message('Граничные значения Y некорректны.')
            x = np.linspace(-10, 10, num=200)
            try:
                for i in self.data_func:
                    self.canvas.axes.plot(x, i, linewidth=2.0)
            except:
                self.error_message('Невозможно произвести действие.')
            else:
                self.canvas.draw()

    # Функция для очистки холста и обнуления списка отображенных функций
    def clear_canvas(self):
        self.data_func = []
        self.canvas.axes.clear()
        self.canvas.draw()

    # Функция формирования окна с сообщением об ошибке
    def error_message(self, text):
        error = QMessageBox()
        error.setWindowTitle('Ошибка!')
        error.setText(text)
        error.setIcon(QMessageBox.Warning)
        error.exec_()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MyWidget()
    ex.show()
    sys.exit(app.exec_())
