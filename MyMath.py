import numpy as np
from sympy import *


class MathForPlot:
    def __init__(self, function):
        self.function = function

    # Функция для вычисления производной функции
    def derivative(self):
        self.function = self.function.replace('ctg', '1/tan')
        return str(diff(self.function))

    # Функция для преобразования входных формул в вид для отображения на холсте
    def plot_graph(self):
        special_formulas = ['sin', 'cos', 'tan', 'log', 'exp', 'pi', 'sqrt']
        x = np.linspace(-10, 10, num=200)
        self.function = self.function.replace('ctg', '1/tan')
        for j in special_formulas:
            self.function = self.function.replace(j, f'np.{j}')
        f = eval(self.function)
        return f
